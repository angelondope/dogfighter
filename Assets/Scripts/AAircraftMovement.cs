﻿using UnityEngine;
using System.Collections;

public class AAircraftMovement : MonoBehaviour {

	public static bool _IsInputEnabled = true;
	public GameObject _Blade;
	public ParticleSystem _LeftWingTrail;
	public ParticleSystem _RightWingTrail;
	public float _BladeRotation;
	public float _TakeOffTime;
	public float _EngineStartTime;
	public float _MinimumSpeed;
	public float _MaxSpeed;
	public float _MinimumNeededSpeed;
	public float _AccelerationPerFrame;
	public float _PlaneTakeOffAngle;
	float mIntitialTime;
	float mFinalTime;
	float mSpeed;
	Vector3 mInitialAngle;
	bool mEngineStarted;
	bool mTookOff = false;

	public delegate void OnRunWay ();
	public event OnRunWay RunOnRunWay; 

	void Awake(){
		mEngineStarted = true;
		RunOnRunWay += RunBeforeTakeOff;
	}

	void Start(){
		mIntitialTime = Time.time;
		mFinalTime = mIntitialTime + _EngineStartTime;
		mInitialAngle = transform.rotation.eulerAngles;
		mSpeed = _MinimumNeededSpeed;
	} 

	IEnumerator TakeOffCR(){
		float initialTime = Time.time;
		float finalTime = initialTime + _TakeOffTime;
		while (Time.time < finalTime) {
			Vector3 forward = Vector3.forward * (Time.time - initialTime)/_TakeOffTime * _MinimumNeededSpeed * Time.deltaTime;
			transform.Translate (forward,Space.World);
			float angle = Mathf.Lerp (mInitialAngle.x, _PlaneTakeOffAngle, (Time.time - initialTime) * 2f/_TakeOffTime);
			transform.rotation = Quaternion.Euler (angle, mInitialAngle.y, mInitialAngle.z);
			yield return null;
		}
		mTookOff = true;
		_IsInputEnabled = true;
		StopCoroutine ("TakeOffCR");
		yield return null;
	}

	void RunBeforeTakeOff(){
		StartCoroutine (TakeOffCR ());
		mEngineStarted = false;
	}

	void Update(){
		float bladeRotationFactor = 1f;
		if (_IsInputEnabled) {
			float rotateAlongZ = Input.GetAxisRaw ("Horizontal");
			float rotateAlongX = Input.GetAxisRaw ("Vertical");
			transform.Rotate (rotateAlongX, 0, -rotateAlongZ);
			if(Input.GetKey(KeyCode.K))
				mSpeed = Mathf.Lerp (mSpeed, _MaxSpeed, _AccelerationPerFrame/(_MaxSpeed - mSpeed - Time.deltaTime));
			if(Input.GetKey(KeyCode.L))
				mSpeed = Mathf.Lerp (mSpeed, _MinimumSpeed, _AccelerationPerFrame/(mSpeed - _MinimumSpeed - Time.deltaTime));
		}
		if (Time.time < mFinalTime) 
			bladeRotationFactor = (Time.time - mIntitialTime) / _EngineStartTime;
		if ((mFinalTime - Time.time) < 0 && mEngineStarted)
			RunOnRunWay ();
		if (mTookOff)
			transform.Translate (transform.forward * mSpeed * Time.deltaTime,Space.World);
		_Blade.transform.Rotate (0, 0, _BladeRotation * bladeRotationFactor * Time.deltaTime);
		if (Mathf.Abs (mSpeed - _MaxSpeed) <= 0.001f)
			SetTrail (true);
		else
			SetTrail (false);
	}

	void SetTrail(bool status){
		if (status) {
			_LeftWingTrail.Play ();
			_RightWingTrail.Play ();
		} else {
			_LeftWingTrail.Stop ();
			_RightWingTrail.Stop ();
		}
	}
}
