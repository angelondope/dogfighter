﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIAircraftMovement : MonoBehaviour {

	public static bool _IsInputEnabled = false;
	public Transform _Target;
	public GameObject _Blade;
	public ParticleSystem _LeftWingTrail;
	public ParticleSystem _RightWingTrail;
	public AIMechineGun _LeftGun;
	public AIMechineGun _RightGun;
	public Camera _View;
	public float _BladeRotation;
	public float _TakeOffTime;
	public float _EngineStartTime;
	public float _MinimumSpeed;
	public float _MaxSpeed;
	public float _MinimumNeededSpeed;
	public float _AccelerationPerFrame;
	public float _MinimumAlertDistance;
	public float _PlaneTakeOffAngle;
	public float _AvoidCollisionDistance;
	public float _TimeStepUp = 0.001f;
	public float _SphereCastRadius;
	public int _NumberOfWaysOut;
	public float _SecondsBeforeFollow;
	public float _SecondsBeforePursue;
	public float _MaintainSpeedLerpFactor;
	public float _MinimumDistanceForArrival;
	public float _PursuitOffset;
	public float _RollSlerpFactor;

	[SerializeField]
	float mSpeed;

	bool mEngineStarted = false;
	float mTimeVisible = 0;
	float mTimeNotVisible = 0;
	float mTimeLocked = 0;
	float mMaintainTime;
	float mToggle = -1f;

	[SerializeField]
	LayerMask mLayerMask;

	void Awake(){
		mSpeed = _MinimumNeededSpeed;
	}

	public IEnumerator StartEngineCR(System.Action StateChangeToTakeOff){
		float initialTime = Time.time;
		float finalTime = initialTime + _EngineStartTime;
		float bladeRotationFactor = 1f;
		while (Time.time < finalTime) {
			bladeRotationFactor = (Time.time - initialTime) / _EngineStartTime;
			_Blade.transform.Rotate (0, 0, _BladeRotation * bladeRotationFactor * Time.deltaTime);
			yield return null;
		}
		mEngineStarted = true;
		StopCoroutine ("StartEngineCR");
		StateChangeToTakeOff ();
		yield return null;
	}

	public void ResetTime(){
		mTimeVisible = 0;
		mTimeNotVisible = 0;
		mTimeLocked = 0;
		mMaintainTime = _MaintainSpeedLerpFactor;
	}

	public void Seek(Vector3 targetPosition){
		Vector3 pos = AvoidCollision ();

		if (pos != Vector3.zero)
			targetPosition = pos;
		
		Vector3 desiredDirection = (targetPosition - transform.position).normalized;
		Vector3 steerVelocity = (desiredDirection + transform.forward).normalized;
		Vector3 newVelocity = Vector3.Slerp (transform.forward, steerVelocity, Time.fixedDeltaTime).normalized;

		mToggle *= -1f;

		if (mToggle == 1) {
			Quaternion initialRoll = Quaternion.LookRotation (transform.forward, transform.up);
			Quaternion finalRoll = Quaternion.LookRotation (transform.forward, newVelocity);
			transform.rotation = Quaternion.Slerp (initialRoll, finalRoll, _RollSlerpFactor);
		}
		else
			transform.rotation = Quaternion.LookRotation(newVelocity,transform.up);

		transform.Translate (transform.forward * mSpeed * Time.fixedDeltaTime, Space.World);
	}

	public Vector3 AvoidCollision(){
		Vector3 spherePosition = transform.position + transform.forward * _AvoidCollisionDistance;
		RaycastHit[] hit = Physics.SphereCastAll (spherePosition, _SphereCastRadius, Vector3.forward, _SphereCastRadius, mLayerMask.value);

		if (hit.Length > 0) {
			Dictionary<Vector3,int> sphereCastHits = new Dictionary<Vector3,int> ();
			sphereCastHits.Add (spherePosition, hit.Length);
			int n = _NumberOfWaysOut * 2;
			while (n-- > 0) {
				Vector3 pos = transform.position + PointOnSphere() * _AvoidCollisionDistance;
				if(Vector3.Dot(transform.forward, pos) > 0)
					sphereCastHits.Add(pos, Physics.SphereCastAll (pos, _SphereCastRadius, Vector3.forward, _SphereCastRadius, mLayerMask.value).Length);
			}

			KeyValuePair<Vector3,int> wayOut = new KeyValuePair<Vector3, int>(Vector3.zero,100);
			foreach (KeyValuePair<Vector3,int> iterator in sphereCastHits) {
				if (iterator.Value < wayOut.Value)
					wayOut = iterator;
			}
			return wayOut.Key;
		}
		return Vector3.zero;
	}

//	void OnDrawGizmos(){
//		Gizmos.color = Color.black;
//		Gizmos.DrawLine (_View.ViewportToWorldPoint (new Vector3 (0.25f, 0.25f, 1f)), _View.ViewportToWorldPoint (new Vector3 (0.75f, 0.25f, 1f)));
//	}

	public bool IsTargetVisibleForSeconds(float time){
		Vector3 screenPoint = _View.WorldToViewportPoint (_Target.position);

		if (screenPoint.x > 0 && screenPoint.x < 1f && screenPoint.y > 0 && screenPoint.y < 1f && screenPoint.z > 0) {
			mTimeVisible += Time.fixedDeltaTime;
			if (mTimeVisible >= time)
				return true;
		} else
			mTimeVisible = 0;
		
		return false;
	}

	public bool IsTargetNotVisibleForSeconds(float time){
		Vector3 screenPoint = _View.WorldToViewportPoint (_Target.position);
		if (screenPoint.x > 0 && screenPoint.x < 1f && screenPoint.y > 0 && screenPoint.y < 1f && screenPoint.z > 0)
			mTimeNotVisible = 0;
		else {
			mTimeNotVisible += Time.fixedDeltaTime;
			if (mTimeNotVisible >= time)
				return true;
		}
		return false;
	}

	public bool IsTargetLockedForSeconds(float time)
	{
		Vector3 screenPoint = _View.WorldToViewportPoint (_Target.position);

		if (screenPoint.x > 0.3f && screenPoint.x < 0.7f && screenPoint.y > 0.3f && screenPoint.y < 0.7f && screenPoint.z > 0) 
		{
			mTimeLocked += Time.fixedDeltaTime;

			if (mTimeLocked >= time)
				return true;
		} 
		else
			mTimeLocked = 0;
		
		return false;
	}

	public void Patrol()
	{
		transform.Translate (transform.forward * mSpeed * Time.fixedDeltaTime, Space.World);
	}

	void Update()
	{
		if(mEngineStarted)
			_Blade.transform.Rotate (0, 0, _BladeRotation * Time.deltaTime);
		
		if (Mathf.Abs (mSpeed - _MaxSpeed) <= 0.001f)
			SetTrail (true);
		else
			SetTrail (false);
	}

	void FixedUpdate()
	{
		Vector3 viewportPosition = _View.WorldToViewportPoint (_Target.position);

		if (viewportPosition.x > 0.43f && viewportPosition.x < 0.57f && viewportPosition.y > 0.43f && viewportPosition.y < 0.57f)
			Fire ();
	}

	void SetTrail(bool status)
	{
		if (status) 
		{
			_LeftWingTrail.Play ();
			_RightWingTrail.Play ();
		} 
		else 
		{
			_LeftWingTrail.Stop ();
			_RightWingTrail.Stop ();
		}
	}

	public Vector3 PointOnSphere()
	{
		float z = 2f * Random.value - 1f;
		float t = 2f * Mathf.PI * Random.value;
		float w = Mathf.Sqrt (1 - z * z);
		float x = w * Mathf.Cos (t);
		float y = w * Mathf.Sin (t);
		return new Vector3 (x, y, z);
	}

	public void Accelerate(bool speedUp)
	{
		if(speedUp)
			mSpeed = Mathf.Clamp (mSpeed += _AccelerationPerFrame, _MinimumSpeed, _MaxSpeed);
		else
			mSpeed = Mathf.Clamp (mSpeed -= _AccelerationPerFrame, _MinimumSpeed, _MaxSpeed);
	}

	public bool MaintainSpeedAndDistance(float distance)
	{
		float targetSpeed = _Target.GetComponent<AircraftMovement> ().pPlayerSpeed;
		float effectiveDistance = DistanceFromTarget () - distance;

		if (effectiveDistance > 0) 
		{
			
			if ((Mathf.Abs (targetSpeed - _MaxSpeed) < 1e-2) && (Mathf.Abs (targetSpeed - mSpeed) < 1e-2))
				return true;
			else if (targetSpeed > mSpeed)
				mSpeed = _MaxSpeed;
			else
				Accelerate (false);
			
		} 
		else 
		{
			
			if (Mathf.Abs (targetSpeed - mSpeed) > 1e-2) {
				float lerpSpeed = Mathf.Lerp (mSpeed, targetSpeed, mMaintainTime += _TimeStepUp);
				mSpeed = Mathf.Clamp (lerpSpeed, _MinimumSpeed, _MaxSpeed);
			} else {
				mSpeed = targetSpeed;
				return true;
			}

		}

		return false;
	}
		
	public float DistanceFromTarget()
	{
		return Mathf.Abs((_Target.position - transform.position).magnitude);
	}

	public void Fire()
	{
		_LeftGun.Fire ();
		_RightGun.Fire ();
	}
}
