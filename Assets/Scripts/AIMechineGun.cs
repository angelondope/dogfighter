﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AIMechineGun : MonoBehaviour {

	public Transform _Bullet;
	public float _DistanceToShootAt;

	public void Fire()
	{
		GameObject bullet = Instantiate (_Bullet.gameObject, transform.position, Quaternion.identity) as GameObject;
		Vector3 target = transform.root.position + transform.root.forward * _DistanceToShootAt;
		BulletLife bulletLife = bullet.GetComponent<BulletLife> ();
		bulletLife._BulletLifeTime = _DistanceToShootAt / bulletLife._BulletSpeed;
		bulletLife._TravelDirection = (target - transform.position).normalized;
		bullet.transform.GetChild (0).rotation = Quaternion.LookRotation (-transform.root.up,transform.root.forward);
	}
}
