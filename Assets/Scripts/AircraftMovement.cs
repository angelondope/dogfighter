﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AircraftMovement : MonoBehaviour {

	public static bool _IsInputEnabled = false;
	public GameObject _Blade;
	public ParticleSystem _LeftWingTrail;
	public ParticleSystem _RightWingTrail;
	public Image _CrossHair; 
	public RectTransform _CrossHairRectTranform;
	public RectTransform _CanvasRectTransform;
	public Camera _PlayerCamera;
	public float _BladeRotation;
	public float _TakeOffTime;
	public float _EngineStartTime;
	public float _MinimumSpeed;
	public float _MaxSpeed;
	public float _MinimumNeededSpeed;
	public float _AccelerationPerFrame;
	public float _PlaneTakeOffAngle;
	public float _BulletRange;

    float mInitialTime = -1;
	float mFinalTime = -1;
    float mBladeRotationFactor = 1f;
    
	Vector3 mInitialAngle;

    bool mEngineStarted = false;
    public bool pEngineStarted
    {
        get { return mEngineStarted; }
    }

    float mSpeed;
    public float pPlayerSpeed
	{
		get{ return mSpeed; }
	}

    Vector3 mCrossHairViewportPosition;
    public Vector3 pCrossHairViewportPosition
	{
		get{ return mCrossHairViewportPosition; }
	}

    private void StartEngine()
    {
        //TODO: on off for home seakers
        //Engine start Audio
        mInitialTime = Time.time;
        mFinalTime = mInitialTime + _EngineStartTime;
        mInitialAngle = transform.rotation.eulerAngles;
        StartCoroutine(EngineReadyCR());
    }

    IEnumerator EngineReadyCR()
    {
        while (Time.time < mFinalTime)
        {
            mBladeRotationFactor = (Time.time - mInitialTime) / _EngineStartTime;
            _Blade.transform.Rotate (0, 0, _BladeRotation * mBladeRotationFactor * Time.deltaTime);
            yield return null;
        }
        mEngineStarted = true;
        mInitialTime = Time.time;
        mFinalTime = mInitialTime + _TakeOffTime;
        StartCoroutine(TakeOffCR());
    }

    IEnumerator TakeOffCR()
	{
		while (Time.time < mFinalTime)
		{
			mSpeed = (Time.time - mInitialTime) / _TakeOffTime * _MinimumNeededSpeed;
            float angle = Mathf.Lerp (mInitialAngle.x, _PlaneTakeOffAngle, (Time.time - mInitialTime) * 2f / _TakeOffTime);
			transform.rotation = Quaternion.Euler (angle, mInitialAngle.y, mInitialAngle.z);
			yield return new WaitForFixedUpdate();
		}

		_IsInputEnabled = true;
        StopAllCoroutines();
	}

	void Update()
	{
        if (Input.GetKey(KeyCode.P))
            StartEngine();

        if (mEngineStarted)
        {
            _Blade.transform.Rotate (0, 0, _BladeRotation * mBladeRotationFactor * Time.deltaTime);
        }

		if (Mathf.Abs (mSpeed - _MaxSpeed) <= 1e-3)
			SetTrail (true);
		else
			SetTrail (false);
	}

	void SetTrail(bool status)
	{
		if (status)
		{
			_LeftWingTrail.Play ();
			_RightWingTrail.Play ();
		} 
		else 
		{
			_LeftWingTrail.Stop ();
			_RightWingTrail.Stop ();
		}
	}

    void FixedUpdate()
    {

        if (_IsInputEnabled)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            if (Input.GetKey(KeyCode.K))
                mSpeed = Mathf.Clamp(mSpeed += _AccelerationPerFrame, _MinimumSpeed, _MaxSpeed);
            if (Input.GetKey(KeyCode.L))
                mSpeed = Mathf.Clamp(mSpeed -= _AccelerationPerFrame, _MinimumSpeed, _MaxSpeed);

            transform.Translate(transform.forward * mSpeed * Time.fixedDeltaTime, Space.World);

            if (horizontal != 0 || vertical != 0)
                transform.Rotate(vertical, 0, -horizontal);
        }
        else if(mEngineStarted)
            transform.Translate(Vector3.forward * mSpeed * Time.fixedDeltaTime, Space.World);

        mCrossHairViewportPosition = Camera.main.WorldToViewportPoint(transform.position + transform.forward * _BulletRange);
        Vector2 crossHairScreenPosition = new Vector2(0, pCrossHairViewportPosition.y * _CanvasRectTransform.sizeDelta.y - _CanvasRectTransform.sizeDelta.y * 0.5f);
        float screenYLimit = _CanvasRectTransform.sizeDelta.y * 0.5f;
        crossHairScreenPosition.y = Mathf.Clamp(crossHairScreenPosition.y, -screenYLimit + screenYLimit * 0.2f, screenYLimit - screenYLimit * 0.2f);
        _CrossHairRectTranform.anchoredPosition = crossHairScreenPosition;
	}
		
}
