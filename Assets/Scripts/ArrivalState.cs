﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrivalState : EnemyState
{
	public ArrivalState(StateMachine stateMachine, AIAircraftMovement aiAircraftMovement):
        base(stateMachine, aiAircraftMovement) { }

	public override void StartState()
    {
		Debug.Log ("Arrival State");
	}
	
	public override void FixedUpdateState(){
 		mAIAircraftMovement.Seek (mAIAircraftMovement._Target.position);
        if (mAIAircraftMovement.MaintainSpeedAndDistance(3f) && mAIAircraftMovement.IsTargetVisibleForSeconds(mAIAircraftMovement._SecondsBeforeFollow))
            ChangeState(mStateMachine._FollowState);
    }
}
