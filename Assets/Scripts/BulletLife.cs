﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BulletLife : MonoBehaviour {

	public float _BulletSpeed = 1f;

	[HideInInspector]
	public float _BulletLifeTime = 1f;
	[HideInInspector]
	public Vector3 _TravelDirection;

	void FixedUpdate () 
	{
		transform.Translate (_TravelDirection * Time.fixedDeltaTime * _BulletSpeed);
		Destroy (gameObject,_BulletLifeTime);
	}
}
