﻿using UnityEngine;
using System.Collections;

public class EnemyState
{
    protected StateMachine mStateMachine;
    protected AIAircraftMovement mAIAircraftMovement;

    public EnemyState(StateMachine stateMachine, AIAircraftMovement aIAircraftMovement)
    {
        mStateMachine = stateMachine;
        mAIAircraftMovement = aIAircraftMovement;
    }

    public virtual void StartState() { }
    public virtual void UpdateState() { }
    public virtual void FixedUpdateState() { }
    public virtual void OnTriggerEnter(Collider other) { }
    public virtual void ToPatrolState() { }
    public virtual void ToArrivalState() { }
    public virtual void ToFollowState() { }
    public virtual void ToPursueState() { }
    public virtual void ToEvadeState() { }

    public virtual void ChangeState(EnemyState state)
    {
        mStateMachine._CurrentState = mStateMachine._FollowState;
        mStateMachine._CurrentState.StartState();
    }
}
