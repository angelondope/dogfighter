﻿using UnityEngine;
using System.Collections;

public class FollowAircraft : MonoBehaviour
{
	public Transform _Aircraft;
	public float _InitialMoveSpeed = 1f;
	public float _FinalMoveSpeed = 3f;
	public float _RotationSpeed = 1f;
	public float _InitialHorizontalOffset;
	public float _FinalHorizontalOffset;
	public float _VerticalOffset;
	public float _SpeedStepUp;
    public float _FinalStepUp;

	float mHorizontalOffset;
    Vector3 mPlayerStillPosition;
	Quaternion mRotationOffset;
	AircraftMovement mAircraftMovement;

    float mMoveSpeed;
    public float pMoveSpeed
	{
		get{ return mMoveSpeed; }
	}

	void Awake()
	{
		mAircraftMovement = _Aircraft.GetComponent<AircraftMovement> ();
		mRotationOffset = transform.rotation * Quaternion.Inverse(_Aircraft.rotation);
	}

	void Start()
	{
		mHorizontalOffset = _InitialHorizontalOffset;
		mMoveSpeed = _InitialMoveSpeed;
        mPlayerStillPosition = _Aircraft.position;
    }

    void FixedUpdate()
	{
		transform.position = Vector3.Lerp (transform.position, _Aircraft.position - _Aircraft.forward * mHorizontalOffset +
			_Aircraft.up * _VerticalOffset, mMoveSpeed);

        if(mAircraftMovement.pEngineStarted)
            mMoveSpeed = Mathf.Lerp(mMoveSpeed, mAircraftMovement.pPlayerSpeed / 4f, _SpeedStepUp);

        if (AircraftMovement._IsInputEnabled) 
		{
            _SpeedStepUp = Mathf.Lerp(_SpeedStepUp, _FinalStepUp, mMoveSpeed);
			Quaternion rotationTarget = _Aircraft.rotation * mRotationOffset;
			transform.rotation = Quaternion.Lerp (transform.rotation, rotationTarget, Time.deltaTime * _RotationSpeed);
		}
	}
}
