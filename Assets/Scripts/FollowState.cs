﻿using UnityEngine;
using System.Collections;

public class FollowState : EnemyState
{

	public FollowState(StateMachine stateMachine, AIAircraftMovement aiAircraftMovement):
        base(stateMachine,aiAircraftMovement){}

	public override void StartState(){
		Debug.Log ("Follow State");
		mAIAircraftMovement.ResetTime ();
	}

	public override void FixedUpdateState(){
		mAIAircraftMovement.Seek (mAIAircraftMovement._Target.position);
		if (mAIAircraftMovement.MaintainSpeedAndDistance (1f)) {
            if (mAIAircraftMovement.IsTargetLockedForSeconds(mAIAircraftMovement._SecondsBeforePursue))
                ChangeState(mStateMachine._PursueState);
            else if (mAIAircraftMovement.IsTargetNotVisibleForSeconds(mAIAircraftMovement._SecondsBeforePursue))
                ChangeState(mStateMachine._ArrivalState);
		}
	}
}
