﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MechineGun : MonoBehaviour {

	public Transform _Bullet;
	public AircraftMovement _AircraftMovement;

	void FixedUpdate()
	{
		if (AircraftMovement._IsInputEnabled && Input.GetKey (KeyCode.J))
			Fire ();
	}

	void Fire()
	{
		GameObject bullet = Instantiate (_Bullet.gameObject, transform.position, Quaternion.identity) as GameObject;
		Vector3 crossHairPosition = Camera.main.ViewportToWorldPoint (_AircraftMovement.pCrossHairViewportPosition);
		Vector3 distanceVector = crossHairPosition - transform.root.position;
		BulletLife bulletLife = bullet.GetComponent<BulletLife> ();
		float time = distanceVector.magnitude / (bulletLife._BulletSpeed - _AircraftMovement.pPlayerSpeed);
		float actualDistance = (_AircraftMovement.pPlayerSpeed + bulletLife._BulletSpeed) * time;
		Vector3 target = transform.root.position + distanceVector.normalized * actualDistance;
		bulletLife._TravelDirection = (target - transform.position).normalized;
		bulletLife._BulletLifeTime = time;
		bullet.transform.GetChild (0).rotation = Quaternion.LookRotation (-transform.root.up,distanceVector.normalized);
	}
}
