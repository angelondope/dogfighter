﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PursueState : EnemyState
{

	public PursueState(StateMachine stateMachine, AIAircraftMovement aiAircraftMovement):
        base(stateMachine, aiAircraftMovement) { }

	public override void StartState(){
		Debug.Log ("Pursue State");
		mAIAircraftMovement.ResetTime ();
	}
	
	public override void FixedUpdateState(){
		Transform target = mAIAircraftMovement._Target;
		float targetSpeed = target.GetComponent<AircraftMovement> ().pPlayerSpeed;
		Vector3 offsetPosition = target.position + target.forward * mAIAircraftMovement._PursuitOffset;
		float aheadTime = (offsetPosition - mAIAircraftMovement.transform.position).magnitude / (targetSpeed + mAIAircraftMovement._MaxSpeed);
		mAIAircraftMovement.Seek (offsetPosition + target.forward * targetSpeed * aheadTime);

		if (mAIAircraftMovement.DistanceFromTarget () > mAIAircraftMovement._PursuitOffset)
			mAIAircraftMovement.Accelerate (true);
		else
			mAIAircraftMovement.MaintainSpeedAndDistance (mAIAircraftMovement._PursuitOffset);

		if (mAIAircraftMovement.IsTargetNotVisibleForSeconds (mAIAircraftMovement._SecondsBeforeFollow))
			ToFollowState ();
	}
}
