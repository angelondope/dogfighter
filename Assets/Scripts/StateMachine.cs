﻿using UnityEngine;
using System.Collections;

public class StateMachine : MonoBehaviour {

	public EnemyState _CurrentState = null;
	public TakeOffState _TakeOffState;
	public PatrolState _PatrolState;
	public ArrivalState _ArrivalState;
	public FollowState _FollowState;
	public PursueState _PursueState;
	private AIAircraftMovement _AIAircraftMovement;

	void Awake(){
		_AIAircraftMovement = GetComponent<AIAircraftMovement> ();
		_TakeOffState = new TakeOffState (this,_AIAircraftMovement);
		_PatrolState = new PatrolState (this,_AIAircraftMovement);
		_ArrivalState = new ArrivalState (this,_AIAircraftMovement);
		_FollowState = new FollowState (this, _AIAircraftMovement);
		_PursueState = new PursueState (this, _AIAircraftMovement);
	}

	void Start () {
		StartCoroutine (_AIAircraftMovement.StartEngineCR (()=>{
			_CurrentState = _TakeOffState;
			_CurrentState.StartState();
		}));
	}

	void Update () {
		if (_CurrentState != null) 
			_CurrentState.UpdateState ();
	}

	void FixedUpdate(){
		if (_CurrentState != null)
			_CurrentState.FixedUpdateState ();
	}

	void OnTriggerEnter(Collider other){
		_CurrentState.OnTriggerEnter (other);
	}
		
}
