﻿using UnityEngine;
using System.Collections;

public class TakeOffState : EnemyState
{
	Transform mAIAircraft;
	float mInitialTime ;
	float mFinalTime ;
	Vector3 mInitialAngle;

	public TakeOffState(StateMachine stateMachine, AIAircraftMovement aiAircraftMovement):
        base(stateMachine, aiAircraftMovement)
    {
		mAIAircraft = mStateMachine.transform;
	}

    public override void StartState()
    {
		Debug.Log ("TakeOff State");
		mInitialTime = Time.time;
		mFinalTime = mInitialTime + mAIAircraftMovement._TakeOffTime;
		mInitialAngle = mStateMachine.transform.rotation.eulerAngles;
	}

    public override void FixedUpdateState()
    {
        if (Time.time < mFinalTime)
        {
            Vector3 forward = -Vector3.forward * (Time.time - mInitialTime) / mAIAircraftMovement._TakeOffTime * mAIAircraftMovement._MinimumNeededSpeed;
            mAIAircraft.Translate(forward * Time.fixedDeltaTime, Space.World);
            float angle = Mathf.Lerp(mInitialAngle.x, mAIAircraftMovement._PlaneTakeOffAngle, (Time.time - mInitialTime) * 2f / mAIAircraftMovement._TakeOffTime);
            mAIAircraft.rotation = Quaternion.Euler(angle, mInitialAngle.y, mInitialAngle.z);
        }
        else
            ChangeState(mStateMachine._ArrivalState);
    }
}
